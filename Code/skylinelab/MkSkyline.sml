functor MkSkyline(structure S : SEQUENCE) : SKYLINE =
struct
  structure Seq = S
  open Primitives
  open Seq

  (* Remove this line when you're done. *)
  exception NotYetImplemented
  exception ThisShouldNotHappen

  (* Datatype determining if something is from the left or right *)
  datatype lor = Left | Right

  (* Similar to Copy function but processes for sequences from Left branch*)
  fun chngHL (prev : (int * int * lor), 
            next : (int * int * lor)) : (int * int * lor) =
    case next of
      (x, y, Right) =>  let val (a,b,c) = prev in
                            (x,b, c) end
                        (* let val (a,b,c) = prev in 
                          case c of
                            Left => (x, b, Left)
                          | Right => (x, b, Right) end *)
    | (x, y, Left) => next 

  (* Similar to Copy function but processes for sequences from Right branch *)
  fun chngHR (prev : (int * int * lor), next : (int * int * lor)) :
                                              (int * int * lor) =
    case next of
      (x, y, Left) =>   let val (a,b,c) = prev in 
                            (x,b, c) end
                        (* let val (a,b,c) = prev in
                           case c of
                            Left => (x, b, Left)
                          | Right => (x, b, Right) end *)
    | (_, _, Right) => next
  
  (* Find sequence of max heights given two sequences *)
  fun maxInSeq (s1 : (int * int * lor) seq, s2 : (int * int * lor) seq) :
                                                (int * int * lor) seq =
    if (length s1 = length s2) then
      tabulate (fn i => let 
                          val (x1, h1, o1) = nth s1 i
                          val (x2, h2, o2) = nth s2 i
                        in
                          if (h1 >= h2) then (x1, h1, o1)
                          else (x2, h2, o2) end) (length s1) 
    else raise ThisShouldNotHappen (* This should never happen *)

  (* Scans the left and right branch and combines by returning points
     with max height for given x point *)
  fun maxscan (ms : (int * int * lor) seq) : (int * int * lor) seq =
    let
      (* Note : Decided to use ~1 since it's possible that a valid height 
                hasn't been found yet when scanning *)
      val basel = let val (l, _, _) = nth ms 0 in (l, ~1, Left) end
      val baser = let val (m, _, _) = nth ms 0 in (m, ~1, Right) end
      val (scanL,scanR) = par(fn () => (scani (chngHL) (basel) ms), 
                              fn () => (scani (chngHR) (baser) ms))
    in
      maxInSeq(scanL, scanR)
    end
  
  (* Adjusted compare function to handle (int * int * lor) types *)
  fun newCmp (a : (int * int * lor), b : (int * int * lor)) : order =
    let
      val (x1, _, _) = a
      val (x2, _, _) = b
    in Int.compare(x1, x2) end 
  
  (* Adjusted compare function to sort buildings : (int * int * int) *)
  fun sortCmp (a : (int * int * int), b : (int * int * int)) : order =
    let
      val (l1, h1, _) = a
      val (l2, h2, _) = b
    in case Int.compare(l1,l2) of
        EQUAL => Int.compare(h1,h2) 
      | _ => Int.compare(l1,l2) 
    end

  fun skyline (buildings : (int * int * int) seq) : (int * int) seq =
      let
        fun computeSkyline (S : (int * int * int) seq) : (int * int) seq =
            case showt S of
              EMPTY => empty() 
            | ELT x => let 
                          val (l, h, r) = x 
                       in %[ (l, h), (r, 0) ] end
              (* Divide Step *)
            | NODE(L, R) => 
                let
                  (* Recurse Step *)
                  val (l, r) = par(fn () => computeSkyline(L),
                                   fn () => computeSkyline(R))
                  (* Tag each point whether from left or right branch *)
                  val (tagL, tagR) = par(fn() => (map (fn x => let 
                                            val (a, b) = x 
                                          in (a, b, Left) end) l),
                                         fn() => (map (fn x => let 
                                            val (a, b) = x
                                          in (a, b, Right) end) r))
                  (* Combine Step *)
                  (* Merge sequence and scan *)
                  val update = maxscan (merge newCmp tagL tagR)
                  (* Convert back to original type : (int * int) *)
                  val convert = map (fn a => let val (x, y, _) = a 
                                                in (x, y) end) update 
                in
                  convert
                end
        val result = computeSkyline(sort sortCmp buildings) 
        (* Filter redundant points *)
        val filtering = filterIdx (fn (i, x) => 
                                if (i <= 0) then true
                                else let val (_, y1) = nth result (i-1)
                                         val (_, y2) = nth result i
                                     in if (y1 = y2) then false
                                        else true
                                     end) result
      in
        filtering 
      end

end
