functor MkRangeCount(structure OrdTable : ORD_TABLE) : RANGE_COUNT =
struct
  structure OrdTable = OrdTable
  open OrdTable

  (* Ordered table type: *)
  type 'a table = 'a table
  type 'a seq = 'a Seq.seq
  type point = Key.t * Key.t

  (* Use this to compare x- or y-values. *)
  val compareKey : (Key.t * Key.t) -> order = Key.compare

  (* Define this yourself *)
  type countTable = Key.t table table

  fun makeCountTable (S : point seq) : countTable =
    if(Seq.length S = 0) then empty()
    else
      let
        (* Sort the keys *)
        val SortS = Seq.sort (fn ((x,y), (x1,y1)) => compareKey (x, x1)) S 
        val (ST, T) = Seq.iterh (fn (B, (x,y)) => insert (#1) (y,x) B) 
                                (empty()) (SortS)
        (* Remove the empty() in the sequence *)
        val NT = Seq.append(Seq.subseq ST (1,Seq.length ST-1),
                                    Seq.singleton(T))
        val K = Seq.map (fn (x,y) => x) SortS
        val Tab = Seq.tabulate (fn i => (Seq.nth K i, Seq.nth NT i)) 
                    (Seq.length NT)
        val FT = fromSeq Tab
      in
        FT 
      end

  fun count (T : countTable)
                   ((xLeft, yHi) : point, (xRght, yLo) : point) : int  =
    let
      val RASize = 
        case (find T xRght) of
          NONE => (case (previous T xRght) of
                      NONE => 0
                    | SOME(k,v) => size((getRange (v) (yLo, yHi))))
          | SOME(v1) => size((getRange (v1) (yLo, yHi)))
      val LASize =  
        case (previous T xLeft) of
          NONE => 0
        | SOME(k1, v1) => size(getRange (v1) (yLo, yHi))
    in
      RASize-LASize
    end

end
