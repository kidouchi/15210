functor MkBSTOrderedTable (structure Tree : BSTREE
                           structure Seq : SEQUENCE) : ORD_TABLE =
struct

  structure Table = MkBSTTable(structure Tree = Tree
                               structure Seq = Seq)

  (* Include all the functionalities of the standard Table *)
  open Table

  (* This is defined after "open" so it doesn't get overwritten *)
  structure Key = Tree.Key
  type key = Key.t

  (* Remember, type 'a table = 'a Tree.bst *)

  fun first (T : 'a table) : (key * 'a) option =
    if(size(T) = 0) then NONE
    else 
      let
        val ROOT = valOf(Tree.expose T)
        (* Recursive function of first *)
        (* W : O(1)  S : O(d) *)
        fun first' (T, (k,v)) =
          if(size(T) = 0) then SOME(k,v)
          else
            let val NEXT = valOf(Tree.expose T)
            in first'(#left NEXT, (#key NEXT, #value NEXT)) end
      in first' (#left ROOT, (#key ROOT, #value ROOT)) end

  fun last (T : 'a table) : (key * 'a) option =
    if(size(T) = 0) then NONE
    else 
      let 
        val ROOT = valOf(Tree.expose T)
        (* Recursive function of last *)
        (* W : O(1)  S : O(d) *)
        fun last' (T, (k,v)) =
          if(size(T) = 0) then SOME(k,v)
          else
            let val NEXT = valOf(Tree.expose T)
            in last'(#right NEXT, (#key NEXT, #value NEXT)) end
      in last'(#right ROOT, (#key ROOT, #value ROOT)) end

  fun previous (T : 'a table) (k : key) : (key * 'a) option =
      let val (L, _, _) = Tree.splitAt(T, k)
      in last(L) end

  fun next (T : 'a table) (k : key) : (key * 'a) option =
      let val (_, x, R) = Tree.splitAt(T, k)
      in first(R) end

  fun join (L : 'a table, R : 'a table) : 'a table =
    Tree.join(L, R)

  fun split (T : 'a table, k : key) : 'a table * 'a option * 'a table =
    Tree.splitAt(T, k)

  fun getRange (T : 'a table) (low : key, high : key) : 'a table =
    if(size(T) = 0) then T
    else 
      let
        val (_, x, LR) = Tree.splitAt(T, low) 
        val (HL, y, _) = Tree.splitAt(LR, high)
      in
        case (x, y) of
          (NONE, NONE) => HL 
        | (NONE, SOME(c)) => join(HL, Tree.singleton(high,c))
        | (SOME(c), NONE) => join(HL, Tree.singleton(low,c))
        | (SOME(c), SOME(c1)) => join(HL, join(Tree.singleton(low, c), 
                                              Tree.singleton(high, c1)))
     end

end
