structure Tests =
struct
  structure Seq = ArraySequence
  open Seq

  type point = int * int

  (* Here are a couple of test structures. ordSet1 is a test ordered set
   * (table), while points1 is a test collection of points in the plane.
   *
   * Note that for an ordered table test, you should just specify a sequence
   * of keys (since that's all that matters). *)

  val ordSet1 = % [5, 7, 2, 8, 9, 1]
  val ordSet2 = % [5]
  val ordSet3 = % [5, 3]
  val ordSet4 = % [5, 6]
  val ordSet5 : int seq = % []
  val ordSet6 = %[5, 7, 2, 8, 9, 1, 4, 6]
  val ordSet7 = %[5, 7, 2, 8, 9, 3]

  val testsFirst = [
    ordSet1, ordSet2, ordSet3, ordSet4, ordSet5, ordSet6, ordSet7
  ]
  val testsLast = [
    ordSet1, ordSet2, ordSet3, ordSet4, ordSet5, ordSet6
  ]
  val testsPrev = [
    (ordSet1, 8),
    (ordSet2, 5),
    (ordSet3, 5),
    (ordSet4, 5),
    (ordSet5, 5),
    (ordSet6, 2),
    (ordSet6, 7)
  ]
  val testsNext = [
    (ordSet1, 8),
    (ordSet2, 5),
    (ordSet3, 5),
    (ordSet4, 5),
    (ordSet5, 5),
    (ordSet6, 2),
    (ordSet6, 7)
  ]
  val testsJoin = [
    (ordSet1, % [100])
  ]
  val testsSplit = [
    (ordSet1, 7),
    (ordSet5, 5)
  ]
  val testsRange = [
    (ordSet1, (5,8))
  ]


  val points1 = % [(0,0),(1,2),(3,3),(4,4),(5,1)]
  val points2 = % [(3,9),(2,4),(5,2),(6,7),(8,5),(10,8),(12,3)]
  val points3 : (int * int) seq = % []

  val testsCount = [
    (points1, ((1,3),(5,1))), (points2, ((5,9),(13,4))), 
    (points2, ((0,8),(1,5))), (points2, ((9,10),(11,3))),
    (points2, ((0,5),(2,3))), (points2, ((3,9),(6,7))),
    (points2, ((5,8), (10,4))), (points3, ((~1,2),(2,1)))
  ]


end
