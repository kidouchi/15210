functor MkAStarCore(structure Table : TABLE
                    structure PQ : PQUEUE
                      where type Key.t = real) : ASTAR =
struct
  structure Set = Table.Set
  structure Seq = Set.Seq

  type weight = real
  type vertex = Set.key
  type edge = vertex * vertex * weight
  type heuristic = vertex -> real

  (* Uncomment this line once you're done *)
  exception NotYetImplemented

  (* Define this type yourself *)
  type graph = weight Table.table Table.table 

  fun makeGraph (E : edge Seq.seq) : graph = 
    let
      val M = Seq.map (fn (x,y,z) => (x, (y,z))) E
      val T = Table.collect M
    in
      Table.map (fn x => Table.fromSeq(x)) T
    end

  fun findPath h G (S, T) : (vertex * real) option = 
    let
      (* Work O(log(|V|)), Span O(log(|V|)*)
      fun N(v) =
        case Table.find G v of 
          NONE => Table.empty()
        | SOME (nbr) => nbr
      
      val SS = Seq.toList(Seq.map (fn x => (0.0, x)) (Set.toSeq(S)))
      val Pq = PQ.fromList(SS)

      (* The work and span is the same as Dijkstra's Algorithm *)
      fun findPath' D Q : (vertex * real) option =
        case (PQ.deleteMin Q) of 
          (NONE, _) => NONE 
        | (SOME (d,v), Q') =>
          if (Set.find T v) then SOME(v, d-h(v))
          else
            case (Table.find D v) of
              SOME _ => findPath' D Q'
            | NONE => 
                let
                  val insert = Table.insert (fn (x, _) => x)
                  val D' = insert (v, d-h(v)) D 
                  fun relax (q, (u,w)) = PQ.insert (d+w+h(u), u) q
                  val Q'' = Table.iter relax Q' (N(v))
                in
                  findPath' D' Q''
                end
    in
      findPath' (Table.empty()) (Pq) 
    end
end
