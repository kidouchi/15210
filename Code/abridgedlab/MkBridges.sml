functor MkBridges(structure STSeq : ST_SEQUENCE) : BRIDGES =
struct
  structure Seq = STSeq.Seq
  open Seq

  type vertex = int
  type edge = vertex * vertex
  type edges = edge seq

  (* Remove these two lines before submittting. *)
  exception NotYetImplemented

  type ugraph = vertex seq seq 

  fun makeGraph (E : edge seq) : ugraph =
    let
      val F = Seq.map (fn (x,y) => (y,x)) E
      val comb = Seq.append (E, F)
      val col = Seq.collect Int.compare comb
    in
      Seq.map (fn (x,y) => y) col
    end

  fun findBridges (G : ugraph) : edges = 
    let
      val xinit = STSeq.fromSeq(tabulate (fn i => false) (length G))
      val start = STSeq.fromSeq(tabulate (fn i => ~1) (length G))
      (* makeDFS: Work O(|V| + |E|), Span O(|V| + |E|) *)
      fun numDFS ((X:bool STSeq.stseq, D:int STSeq.stseq, c:int), v:vertex) =
        if(STSeq.nth X v) then (X, D, c) (* Touch *) 
        else 
          let
            val X' = STSeq.update (v, true) X
            val D' = STSeq.update (v, c) D  (* Enter *)
            val (X'', D'', c') = iter (numDFS) (X', D', c+1) (nth G v) 
          in
            (X'',D'', c')
          end  
      val (_, numdfs, _) = numDFS ((xinit, start, 0), 0) 
      (* Tried to connect dummy node to all vertex in G *)
      val dummyConnect = tabulate (fn i => i) (length G)
      val G' = Seq.append(G, %[dummyConnect]) 

      (* Work O(|V| + |E|), Span O(|V| + |E|) *)
      fun findB (p:vertex) ((X:bool STSeq.stseq, F:edge list, 
                                  M:int STSeq.stseq), v:vertex) =
        if (STSeq.nth X v) then
          let
            val pen = STSeq.nth numdfs p
            val ven = STSeq.nth numdfs v
            val minv = STSeq.nth M ven
            val M' = STSeq.update (ven, Int.min(pen,minv)) M
          in
            (X, F, M') 
          end 
        else 
          let
            val X' = STSeq.update (v, true) X
            val C = filter (fn x => if (x=p) then false else true) (nth G v)
            val en = STSeq.nth numdfs v
            val pen = STSeq.nth numdfs p
            val enns = tabulate (fn i => STSeq.nth numdfs (nth C i)) (length C) 
            val minchild = Int.min(en, (reduce Int.min (length G) enns))
            val minfound = STSeq.nth M en
            val M' = STSeq.update (en, Int.min(minchild, minfound)) M
            val test = STSeq.toSeq M'
            val (X'', F', M'') = iter (findB v) (X', F, M') (C)
            val minexit = STSeq.nth M'' en
            val parentv = STSeq.nth M'' pen
            val M''' = STSeq.update (pen, Int.min(parentv, minexit)) M''
            val F'' = if (minexit = en andalso parentv = pen) 
                      then (p,v) :: F' else F'
          in 
            (X'', F'', M''')
          end
      val xinit1 = STSeq.fromSeq(tabulate (fn i => false) (length G))
      val Min = STSeq.fromSeq(tabulate (fn i => length G) (length G))
      val child = nth (nth G 0) (0)
      val (_, Es, _) = findB (0) ((xinit1, [], Min), child)
    in
      (Seq.fromList(Es)) 
    end 



end
