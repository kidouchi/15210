structure Tester =
struct
  open ArraySequence

  structure Bridges : BRIDGES =
    MkBridges(structure STSeq = MkSTSequence(structure Seq = ArraySequence))

  functor MkAStar(structure Vtx : HASHKEY) : ASTAR =
    MkAStarCore(structure Table = MkTreapTable(structure HashKey = Vtx)
                structure PQ = MkSkewBinomialHeapPQ(structure OrdKey = RealElt))

  structure IntAStar : ASTAR =
    MkAStar(structure Vtx = IntElt)

  structure StringAStar : ASTAR =
    MkAStar(structure Vtx = StringElt)

  val graph = Bridges.makeGraph(%[(0,1),(1,2),(2,3),(0,2),(0,4),(4,6),(1,3),
                                  (4,9),(6,5),(4,5),(6,7),(6,8),(7,8)])

  (* val graph2 = Bridges.makeGraph(%[(5,7),(5,2),(7,0),(2,7),(2,0),(5,9),(9,4),
                                   (9,3),(9,8),(3,8),(6,8),(1,6),(8,1)])
 
  *)

  val graph2 = Bridges.makeGraph(%[(0,1)])

  (* val test1 = Seq.sort sortfn (%[(0,4), (4,9)]) *)
  
  val _ = Bridges.findBridges(graph)
  val _ = Bridges.findBridges(graph2)
  (*
  val _ = Bridges.findBridges(graph2)
  *)

end
