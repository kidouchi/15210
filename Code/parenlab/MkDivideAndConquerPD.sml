functor MkDivideAndConquerPD (structure P : PAREN_PACKAGE) : PAREN_DIST =
struct
  structure P = P
  open Primitives
  open P
  open Seq

  (* Remove this line when you're done. *)
  exception NotYetImplemented

  (* parenDist : paren seq -> int option *)
  (* We split the sequence and branch off into a left and right branch. Then
     when we recurse and then work our way back up, we are passing the following     information :
      1.number of open unmatched, 
      2.number of closed unmatched, 
      3.distance from end of seq to first open paren,
      4.distance from start of seq to first cloed paren,
      5.the max matched distance 
     We combine the results from the left and right branch and we'll finally 
     get our final result
   *)
  fun parenDist (parens : paren seq) : int option =
    let
      fun pd (s : paren seq) : (int * int * int * int * int) =
        case showt(s) of
          EMPTY => (0, 0, 0, 0, 0)
        | ELT OPAREN => (1, 0, 1, 0, 0)
        | ELT CPAREN => (0, 1, 0, 1, 0)
        | (* The split which is the first step of the d&c algorithm *)
          NODE(l, r) =>
            let
              val ((lopen, lclose, lopenDist, lcloseDist, lmax), 
                    (ropen, rclose, ropenDist, rcloseDist, rmax)) = 
                    (* recurse
                       the second step of the d&c algorithm *)
                    par (fn () => pd l, fn () => pd r) 
              (* combine the left half and right half of the sequence
                             the last step of the d&c algorithm *)
              (* Ignoring the number of matched parentheses and updating 
                 the new number of unmatched open and closed paren
                 and then trying to update the distance of the open paren 
                 distance and closed paren distance because we've merged*)
              val res = if(lopen > rclose) then
                (lopen - rclose + ropen, lclose, lopenDist + (length r), 
                 lcloseDist, Int.max(lmax, rmax))
              else if (lopen < rclose) then
                (ropen, rclose - lopen + lclose, ropenDist, 
                  rcloseDist + (length l), Int.max(lmax, rmax))
              (* The case where the current sequence is matched and 
                 we can update as the max matched paren length *)
              else
                (ropen, lclose, ropenDist, lcloseDist, 
                  Int.max(Int.max(lmax, rmax), lopenDist + rcloseDist))
            in
              res
            end
      val (_, _, _, _, max) = pd(parens)
    in
      if(max = 0) then NONE
      else SOME(max)
    end

end
