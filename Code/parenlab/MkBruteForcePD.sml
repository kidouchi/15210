functor MkBruteForcePD (structure P : PAREN_PACKAGE) : PAREN_DIST =
struct
  structure P = P
  open P
  open Seq

  (* Remove this line when you're done. *)
  exception NotYetImplemented

  (* parenMatch : paren seq -> bool *)  
  fun parenMatch (p : paren seq) : bool =
    let 
      fun pm ((NONE, _) | (SOME 0, CPAREN)) = NONE
        | pm (SOME c, CPAREN) = SOME(c-1)
        | pm (SOME c, OPAREN) = SOME(c+1)
    in
      iter pm (SOME 0) p = (SOME 0)
    end 

  (* Helper Function *)
  (* An additional function to check to matched parenthesis *)
  fun isMatched (p : paren seq) : bool = 
    if(length p > 2 andalso parenMatch(p)) then 
      let
        val rmOuter = subseq p (1, length p - 2)
      in  
        parenMatch(rmOuter)
      end
    else if(length p = 2) then parenMatch(p)
    else false

  fun allPossible (p : paren seq) : paren seq seq =
    let
      val plen = length p
    in
      flatten(tabulate (fn a=>tabulate (fn b=>subseq p (a,b)) (plen-a+1)) plen)
    end

  (* parenDist : paren seq -> int option *)
  (* The function, thanks to a helper function, creates all possible sub 
    sequence of the give parenthesis sequence. Then with filter all the sub 
    sequences and only keep sequences that are closed. Lastly, it finds the 
    longest sequence and returning its length. This is a brute force solution 
    because it first creates all possible sequences, filters the sequences for 
    possible solutions, and then finally it returns the length of the longest 
    sequence*)
  fun parenDist (parens : paren seq) : int option =
    let
      (* Creating all possible subsequences of parens *)
      val parenSeq : paren seq seq = allPossible(parens)
      (* Filtering out any unmatched parentheses *)
      val matchSeq : paren seq seq = filter (isMatched) parenSeq
      fun maxDist (i : int option, p : paren seq) : int option =
          let
            val plen = length p
          in
            if(valOf(i) <= plen) then SOME(plen)
            else i
          end
      (* Find the max length which is our best solution *)
      val maxdist = iter maxDist (SOME 0) matchSeq
    in
      if(valOf(maxdist) = 0) then NONE
      else maxdist
    end
    


end
