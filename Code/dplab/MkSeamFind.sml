functor MkSeamFind(structure Seq : SEQUENCE) : SEAMFIND =
struct
  structure Seq = Seq
  open Seq

  type pixel = { r : real, g : real, b : real }
  type image = { width : int, height : int, data : pixel seq seq }
  type gradient = real

  fun findSeam (G : gradient seq seq) : int seq =
    let
      val height = length G 
      val width = length (nth G 0)

      (* Work : O(1) Span : O(1) *)
      (* Finds the minimum adjacent element in the table given the column
          and row *)
      fun minJ (S) (j) : (real * int) =
        if (j=0) then 
          if (nth S j <= nth S (j+1)) then (nth S j, j)
          else (nth S (j+1), j+1)
        else if(j=width-1) then 
          if(nth S j <= nth S (j-1)) then (nth S j,j) 
          else (nth S (j-1), j-1)
        else 
          if(nth S j <= nth S (j+1)) then
            if(nth S j <= nth S (j-1)) then (nth S j,j) 
            else (nth S (j-1), j-1)
          else 
            if(nth S (j+1) <= nth S (j-1)) then (nth S (j+1), j+1) 
            else (nth S (j-1), j-1)

      (* W : O(width) S : O(1) *)
      (* Updates a given row in the gradient table *)
      fun updateRow (P) (i) = 
        let val GRow = nth G i
        in Seq.tabulate (fn j => let val (v, _) = minJ P j in 
                                    (v + nth GRow j) end) width end

      (* W : O(width*height) S : O(1) *)
      (* Updates to a modified gradient table *)
      fun updateG (G') (i) = 
        if(i = height) then G'
        else let val newG = Seq.inject 
                    (Seq.singleton(i, updateRow (nth G' (i-1)) i)) G'
              in updateG (newG) (i+1) end

      val G' = updateG G 1
     
      (* Finds seam of lowest cost *)
      (* W : O(height) S : O(height) *)
      fun findLCSeam (i) (L) : int list =
        case L of
          [] =>
            let 
              val Last = nth G' i
              val startc = Seq.reduce (fn (a,b) => 
                            if(nth Last a <= nth Last b) then a 
                            else b) (0) (Seq.tabulate (fn x => x) width)
              in startc::(findLCSeam (i-1) (startc::L)) end
        | x::L' => 
            if(i = 0) then let val (_, c) = minJ (nth G' i) x in [c] end
            else
              let val (_, nextc) = minJ (nth G' i) (x)
              in nextc::(findLCSeam (i-1) (nextc::L)) end
    in
       Seq.rev(Seq.fromList(findLCSeam (height-1) ([])))
    end
end
