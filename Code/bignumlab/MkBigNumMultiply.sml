functor MkBigNumMultiply(structure BNA : BIGNUM_ADD
                         structure BNS : BIGNUM_SUBTRACT
                         sharing BNA.Util = BNS.Util) : BIGNUM_MULTIPLY =
struct
  structure Util = BNA.Util
  open Util
  open Seq

  (* Remove this line when you're done. *)
  exception NotYetImplemented

  infix 6 ++ --
  infix 7 **

  fun x ++ y = BNA.add (x, y)
  fun x -- y = BNS.sub (x, y)

  fun (x : bignum) ** (y : bignum) : bignum =
    let 
      val maxLen = Int.max(length x, length y)
      (* Pad sequence with zeroes *) 
      val paddX = tabulate (fn i => if (i < length x) then nth x i else ZERO)
                    (maxLen)
      val paddY = tabulate (fn i => if (i < length y) then nth y i else ZERO)
                    (maxLen)
      (* Helper function that shifts the bits over n times *)
      fun shift (S : bignum, n : int) : bignum = 
        tabulate (fn i => if (i < n) then ZERO
                          else nth S (i-n)) (n + length S)
    in    
        (* Split sequence *)
        case (showt(paddX), showt(paddY)) of
          (ELT(a), ELT(b)) =>  (case (a,b) of
                                (ZERO, _) => singleton(ZERO)
                              | (_, ZERO) => singleton(ZERO)
                              | (ONE, ONE) => singleton(ONE))
        | (NODE(Q, P), NODE(S, R)) =>
            let
              val n = 2 * length Q
              (* Recurse *)
              val (pr, pqrs, qs) = Primitives.par3 (fn() => P ** R,
                                        fn() => (P++Q) ** (R++S),
                                        fn() => Q ** S)
              val psrq = pqrs -- pr -- qs
              val spsrq = shift(psrq, n div 2)
              val spr = shift(pr, n)
              val mult = (spr ++ spsrq ++ qs)
              (* Remove trailing zeroes *)
              val tagI = mapIdx (fn (i, x) =>
                                case x of
                                  ZERO => 0
                                | ONE => i+1) mult
              val maxInt = reduce Int.max 0 tagI
            in
              subseq mult (0, maxInt)
            end
        |   _ => singleton(ZERO)
    end 
  
  
  val mul = op**

end
