functor MkBigNumSubtract(structure BNA : BIGNUM_ADD) : BIGNUM_SUBTRACT =
struct
  structure Util = BNA.Util
  open Util
  open Seq

  (* Remove this line when you're done. *)
  exception NotYetImplemented

  infix 6 ++ --

  fun x ++ y = BNA.add (x, y)
  
  (* Assume x >= y, result always non-negative *)
  fun (x : bignum) -- (y : bignum) : bignum =
    let
      val maxLen = Int.max(length x, length y) + 1
      (* Pad sequence with zeroes *)
      val paddY = tabulate (fn i => if (i < length y) then nth y i else ZERO)
                      (maxLen+1) 
      val paddX = tabulate (fn i => if (i < length x) then nth x i else ZERO)
                      (maxLen)
      (* Negate y by flipping bits and adding 1 *)
      val negateY = map (fn a => case a of ZERO => ONE | ONE => ZERO) paddY
      val subtract = x ++ (negateY ++ %[ONE]) 
      val res = subseq subtract (0, length subtract - 1)
      (* Remove trailing zeroes *)
      val tagI = mapIdx (fn (i, x) =>   
                                case x of
                                  ZERO => 0
                                | ONE => i+1) res
      val maxInt = reduce Int.max 0 tagI
    in
      subseq res (0, maxInt)
    end
  

  val sub = op--
end
