functor MkBigNumAdd(structure U : BIGNUM_UTIL) : BIGNUM_ADD =
struct
  structure Util = U
  open Util
  open Seq

  (* Remove this line when you're done. *)
  exception NotYetImplemented

  infix 6 ++

  datatype carry = GEN | PROP | STOP

  fun addScan (A : carry, B : carry) : carry =
    case (A, B) of
      (GEN, PROP) => GEN
    | (GEN, GEN) => GEN
    | (GEN, STOP) => STOP
    | (PROP, PROP) => PROP
    | (PROP, GEN) => GEN
    | (PROP, STOP) => STOP
    | (STOP, PROP) => STOP
    | (STOP, GEN) => GEN
    | (STOP, STOP) => STOP
 
  (* Helper function that  adds two sequences without carrying *)
  fun addNoCarry (s : (bit * bit) seq) : bit seq =
    map (fn (x,y) => case (x,y) of 
                        ((ZERO,ZERO) | (ONE,ONE)) => ZERO 
                     |  _ => ONE) s
  
  fun (x : bignum) ++ (y : bignum) : bignum =
    let
      val maxLen = Int.max(length x, length y) + 1
      (* Pad sequence with zeroes *) 
      val newX : bignum = tabulate (fn i => if(i < length x) then nth x i
                                            else ZERO) (maxLen)
      val newY : bignum = tabulate (fn i => if(i < length y) then nth y i
                                            else ZERO) (maxLen)
      val tupleSeq = zip newX newY
      val toCarry : carry seq = map (fn (x,y) => case (x,y) of
                                        (ZERO,ZERO) => STOP
                                     |  (ONE,ONE) => GEN
                                     |   _  => PROP) tupleSeq
      (* Scan to help keep track of what is carried over *)
      val (carryScan, _) = scan addScan STOP toCarry
      val addSeq = addNoCarry(tupleSeq)
      val carryToNum = map (fn x => case x of STOP => ZERO | _ => ONE) 
                                                             carryScan
      val resZip = zip addSeq carryToNum
      val resSeq = addNoCarry(resZip)
      (* Remove trailing zeroes *)
      val tagI = mapIdx (fn (i, x) => 
                                case x of
                                  ZERO => 0
                                | ONE => i+1) resSeq 
      val maxInt = reduce Int.max 0 tagI
    in
      subseq resSeq (0, maxInt)
    end

  val add = op++
end
