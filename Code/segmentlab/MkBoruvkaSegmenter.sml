functor MkBoruvkaSegmenter
  (structure Seq : SEQUENCE
   structure Rand : RANDOM210
   sharing Seq = Rand.Seq)
  : SEGMENTER =
struct
  structure Seq = Rand.Seq
  open Seq

  structure R = Rand
  type vertex = int
  type weight = int
  type edge = vertex * vertex * weight
  
  type label = vertex * vertex
  type elabel = vertex * vertex * weight * label

  (* Remove this exception when you're done! *)
  exception NotYetImplemented

  (* Find the minimum weighted edge for each vertex *)
  (* Work : O(m+n)  Span O(1) *)
  fun minEdges (sE : elabel seq, vlen : int) : elabel seq = 
    let
      val reorgS = Seq.map (fn (u,v,w,l) => (u, (u,v,w,l))) sE
      val injectS = Seq.tabulate (fn _ => (0,0,0,(0,0))) (vlen)
    in
      Seq.inject reorgS injectS
    end
  
  (* Star Contraction *)
  (* Work : O(n^2 in m)  Span : O(log(n)) *)
  fun minStarContract(V, n:int, E:elabel seq, F:int seq) =
    let
      (* Flips to return heads or tails *)
      (* Tails = 0, Heads = 1 *)
      (* Work : O(n)  Span : O(log(n)) *)
      fun heads (v : vertex, flips : int seq) : int = 
        nth flips v 
      val minE = minEdges(E, n)
      val P = Seq.map (fn (u,v,w,l) => 
                          if(heads(u,F) = 0 andalso heads(v,F) = 1) 
                          then (u,v,w,l) else (u,v,~1,l)) minE
      val V' = Seq.map (fn (c, v) => let val (_,_,z,_) = nth P c in
                                      if(z = ~1) then (c,v) else (c,~1) end) V
      val V'' = Seq.filter (fn(_,x) => (x <> ~1) ) V'
    in
      (V'', P)
    end
  
  (* Work : O(mlog^2(n)) Span : O(log^3(n)) *)
  fun findSegments (E, n) initialCredit : (vertex seq * edge seq) =
    let
      (* Recursive helper function *)
      fun findSegments'((V, ES), (R,C), T, i) =
        if(length ES = 0) then (R, T)
        else 
          let
            val (Flips, _) = Rand.flip (Rand.fromInt(i)) n 
            val (V', PT) = minStarContract(V, n, ES, Flips)
            val P1 = Seq.map (fn (u,v,w,_) => (u,v)) PT
            val injectV = Seq.map (fn (c,_) => (c,(c,c))) V'
            val P = Seq.inject injectV P1
            val filtT' = Seq.filter (fn(_,_,w,_)=> (w <> ~1) ) PT 
            val T' = Seq.map (fn (_,_,w,(u,v)) => (u,v,w)) filtT'
            val mapE' = Seq.map (fn (u,v,w,l) => 
                      let val (_, Pu) = nth P u 
                          val (_, Pv) = nth P v
                      in if(Pu<>Pv) then (Pu,Pv,w,l) else (u,v,~1,l) end) ES
            val E' = Seq.filter(fn(_,_,z,l) => (z <> ~1) ) mapE' 
            (* ------------------ Segmenting Step ------------------ *)
            val PTorg = Seq.map (fn(u,v,w,_) => (v, (u,w))) filtT'
            val Group = Seq.collect Int.compare PTorg
            (* Pixel updated *)
            val R' = Seq.map (fn x => let val (_, px) = nth P x in px end) R
            (* Credit update *)
            val injectC = Seq.map (fn (v,s) => 
                      let val CSS = Seq.map (fn (u,_) => nth C u) s
                          val CS = Seq.append(singleton(nth C v), CSS)
                          val W = Seq.map (fn (_,w) => w) s
                          (* Sum edge weights *)
                          val sumW = Seq.reduce (op+) 0 W
                          (* Find local min credits *)
                          val minCred = Seq.reduce Int.min (initialCredit) CS
                          val newCred = minCred - sumW
                      in
                          (v, newCred)
                      end) Group
            val C' = Seq.inject injectC C
            (* Filter edges based on if credits are >= 0 *)
            val E'' = Seq.filter (fn (u,v,w,_) =>
                if((Int.min(nth C' u, nth C' v) - w) >= 0) then true 
                                        else false) E'
            in
            findSegments'((V',E''), (R',C'), Seq.append(T,T'), i+1)
          end
      val V = Seq.tabulate (fn c => (c,c)) n 
      val R = Seq.tabulate (fn c => c) n
      val C = Seq.tabulate (fn _ => initialCredit) n
      val newE = Seq.map (fn (u,v,w) => (u, v, w, (u,v))) E
      val sortE = Seq.rev(Seq.sort (fn ((_,_,w,_),(_,_,w',_)) =>
                                  Int.compare(w,w')) newE)
      val (IMG, MST) = findSegments'((V,sortE), (R,C), Seq.empty(), 0) 
    in   
      (IMG, MST)
    end


end
