functor MkAllShortestPaths (Table : TABLE) : ALL_SHORTEST_PATHS =
struct
  open Table
  open Seq

  (* Remove the following when you're done! *)
  exception NotYetImplemented
  type nyi = unit

  (* Table.key defines our vertex type *)
  type vertex = key
  type edge = vertex * vertex

  (* Keys are all the vertices in the graph
   * Values are the vertices neighbors *)
  type graph = (Set.set Table.table)
  
  (* Keys are some child vertex 
   * Values are its parents *)
  type asp = (vertex * Set.set Table.table)

  fun makeGraph (E : edge seq) : graph =
      Table.map (fn v => Set.fromSeq(v)) (Table.collect E)

  fun numEdges (G : graph) : int =
    length (Seq.flatten (map (fn (x,y) => Set.toSeq(y)) (Table.toSeq(G))))

  fun numVertices (G : graph) : int =
    let 
      val red = Seq.reduce Set.union (Set.empty()) (Table.range(G))
      val comb = Set.union(Table.domain(G), red)
    in
      Set.size(comb)
    end 

  fun outNeighbors (G : graph) (v : vertex) : vertex seq =
    case (Table.find G v) of
      NONE => empty()
    | SOME(c) => Set.toSeq(c)

  (* Helper Function *)
  (* Works like outNeighbors without converting it to a sequence *)
  fun outerN (G : graph) (v : vertex) : Set.set =
        case (Table.find G v) of
          NONE => Set.empty()
        | SOME(c) => c

  (* Maps all neighbors (children) of frontier to their parents*)
  fun N (G : graph) (F : Set.set) =
        let
          fun tagN v =
            Table.tabulate (fn k => Set.singleton(v)) (outerN G v)
          val ngh = Table.tabulate tagN F
          (* merge parents *)
          fun m (A,B) = 
            Table.merge (fn (x,y) => Set.union(x,y)) (A,B) 
        in
          Table.reduce (m) (Table.empty()) ngh
        end
  
  fun makeASP (G : graph) (v : vertex) : asp =
    let
      (* A helper recursive function for makeASP *)
      fun helpASP (X : Set.set Table.table, F :  Set.set Table.table) 
                            (G : graph) : Set.set Table.table =
        if (Table.size(F) = 0) then X
        else 
          let
            (* Visited set *)
            val X' = Table.merge (fn (x,y) => x)  (X, F) 
            val n = N G (Table.domain F)
            (* Frontier set *)
            val F' = Table.erase(n, Table.domain X')
          in
            helpASP (X', F') G
          end
    in  
      (v, helpASP (Table.empty(), Table.singleton(v,Set.empty())) G)
    end 

  (* Sorry I changed the variable name for understanding *)
  fun report (A : asp) (u : vertex) : vertex seq seq =
    let
      val (v, T) = A
      fun helpR (v : vertex, u : vertex) (T' : graph) : vertex list seq =
        case (Table.find T' u) of
          NONE => Seq.empty()
        | SOME(p) =>
            if(Set.size(p) = 0) then Seq.singleton [u]
            else 
              let
                val parents = Set.toSeq(p)
                val travup = Seq.map (fn x => 
                               Seq.map (fn L => u::L) (helpR(v, x) T')) parents
                val newpath = Seq.flatten travup
              in
                newpath
              end
    in
      Seq.map (fn x => Seq.rev(Seq.fromList(x))) (helpR (v,u) T) 
    end

end
