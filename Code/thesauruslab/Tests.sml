structure Tests =
struct
  structure T = MkTreapTable(structure HashKey = IntElt)
  structure S = T.Seq
  open S

  type edge = int * int

  val testfile = "support/thesaurus.txt"

  (* A trivial test that has a graph containing 2 vertices and an edge *)
  val edgeseq = [(1,2), (2,3), (1,3)]
  val edgeseq1 = [(1,2), (2,4), (4,1), (2,3), (4,3)]
  (* Tests *)
  
  val testsNum = [edgeseq, edgeseq1];

  val testsOutNeighbors = [(edgeseq, 1)]

  val testsReport = [((edgeseq, 1), 2), ((edgeseq1, 1), 3)]

  val testsNumWords =  [testfile]

  val testsSynonyms = [(testfile, "HELLO")]

  val testsQuery = [(testfile, ("GOOD", "BAD")), 
                    (testfile, ("EARTHLY", "POISON")), 
                    (testfile, ("FUN", "MISERY")),
                    (testfile, ("PALINDROME", "SQUEAMISH"))]
end
