functor MkThesaurusASP (ASP : ALL_SHORTEST_PATHS where type vertex = string)
  : THESAURUS =
struct
  structure Seq = ASP.Seq
  open Seq

  (* Remove the following two lines when you're done! *)
  exception NotYetImplemented
  type nyi = unit

  (* You must define the following type *)
  type thesaurus = (ASP.graph)

  fun make (S : (string * string seq) seq) : thesaurus =
    let
      val E = Seq.tabulate (fn i => let val (w, n) = nth S i in
              Seq.tabulate (fn j => (w, nth n j)) (length n) end) (length S)
    in
      ASP.makeGraph(Seq.flatten(E))
    end

  fun numWords (T : thesaurus) : int =
    ASP.numVertices(T)

  fun synonyms (T : thesaurus) (w : string) : string seq =
    ASP.outNeighbors T w

  fun query (T : thesaurus) (w1 : string) : string -> string seq seq =
    let 
      val masp = (ASP.makeASP T w1)
    in
      (fn x => ASP.report masp x) 
    end 
end
