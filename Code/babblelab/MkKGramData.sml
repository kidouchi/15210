functor MkKGramData
  (structure Util : SEQ_UTIL
   structure Tok : TOKEN
   structure Table : TABLE where type Key.t = Tok.token Tok.Seq.seq
   sharing Table.Seq = Util.Seq
   sharing Table.Seq = Tok.Seq)
  : KGRAM_DATA =
struct
  structure Seq = Util.Seq
  structure Tok = Tok
  open Seq

  (* remove this line before submitting *)
  exception NotImplemented

  type 'a hist = ('a * int) seq
  type token = Tok.token
  type kgram = token seq

  (* redefine this type *)
  (* The table is a histogram because for each k-1 gram as our key 
   * we want to know how many tokens can come after and it frequency 
   * We also keep track of what the length of the gram which is k
   * Then we want the count and frequency of the k-1 grams so 
   * we store a histogram for the keys in table as well *)
  type kgramdata = (token hist Table.table * int * kgram hist)

  (* implement the following functions *)
  fun makeData (corpus : string) (k : int) : kgramdata =
    let
      val tokens : kgram = Tok.tokenize corpus
      val lent = length tokens
      (* Create our keys and values for the table *)
      val tocollect = tabulate (fn i => (subseq tokens (i,k-1), 
                              nth tokens (i+k-1)) )  (lent-(k-1))
      val collected = Table.collect tocollect
      (* Histogramify our results after collecting *)
      val restable = Table.map (fn x => Util.histogram (Tok.compare) x) 
                                                (collected)
      val keys = map (fn (a,b) => a) (Table.toSeq(restable))
      val histo = Util.histogram (Seq.collate Tok.compare) keys 
    in
      (restable, k, histo)
    end
  
  fun lookupHist (data : kgramdata) (gram : kgram) : token hist option =
    let
      val (t, k, _) = data
      (* Find if gram exists in table *)
      val tv = case (Table.find t gram) of NONE => empty() | SOME(c) => c
    in
      (* If gram not found then give back NONE *)
      if (length tv = 0) then NONE
      else if (not (length gram = k-1)) then NONE
      else SOME(tv)
    end

  fun getK (data : kgramdata) =
    let val (_, k, _) = data in k end 

  fun chooseStarter (data : kgramdata) (r : real) : kgram =
    let val (_, _, h) = data in Util.choose h r end

end
