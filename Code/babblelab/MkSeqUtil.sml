functor MkSeqUtil(structure Seq : SEQUENCE) : SEQ_UTIL =
struct
  structure Seq = Seq
  open Seq

  (* remove this line before submitting *)
  exception NotImplemented

  type 'a hist = ('a * int) seq

  fun histogram (cmp : 'a ord) (s : 'a seq) : 'a hist =
    map (fn x => let val (a, s) = x in (a, length s) end)
                  (collect cmp (map (fn x => (x,1)) s))
      
  fun choose (hist : 'a hist) (p : real) : 'a =
    let 
      val (bc, _) = nth hist 0
      val (a, denominator) = reduce (fn ((a,n),(a',n')) => (a',n+n')) 
                                        (bc, 0) hist
      val rdenom = Real.fromInt(denominator)
      val rhist = map (fn (a,b) => (a, Real.fromInt(b))) hist
      val numerl = scani (fn ((a,r),(b,r')) => (b, r + r')) (bc, 0.0) rhist
      val (l, r) = reduce (fn ((a,n), (a',n')) => 
                    let val mag = Real.abs(p - (n'/rdenom))
                        val mag2 = Real.abs(p - (n/rdenom))
                    in if(mag <= mag2) then (a', n')
                       else (a, n) end) (bc, 1.0) numerl
    in 
      l
    end

end
