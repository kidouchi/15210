functor MkBabble
  (structure Rand : RANDOM210
   structure Data : KGRAM_DATA
   structure Util : SEQ_UTIL
   sharing Data.Seq = Util.Seq
   sharing Data.Seq = Rand.Seq)
  : BABBLE =
struct
  structure Seq = Data.Seq
  structure Rand = Rand
  structure Data = Data
  open Seq

  (* remove this line before submitting *)
  exception NotImplemented

  type sentence = Data.token Seq.seq

  (* Implement the following functions *)
  fun makeSentence (data : Data.kgramdata) (len : int) 
                                      (seed : Rand.rand) : sentence  =
    let
      val (rr, _) = Rand.randomReal seed (SOME(0.0, 1.0))
      val k = Data.getK(data)
      val kstart = (Data.chooseStarter data rr)
      val emptyseq = tabulate (fn i => empty()) (len - (k- 1))
      val makeSent = iter (fn (a,b) => 
                  let
                    val tokgram = if ((length a) <= (k-1)) then a 
                                  else subseq a ((length a) - (k-1), k-1)
                    (* Lookup up table for possible kth gram *)
                    val lookup = Data.lookupHist data tokgram
                    val thist = case (lookup) of
                                  NONE => empty()
                                | SOME(c) => c
                    val (rani, _) = Rand.randomReal seed (SOME(0.0, 1.0)) 
                    (* Choose the random kth token for our kgram *)
                    val kthg = Util.choose thist rani
                    val attach = 
                        case (lookup) of
                          NONE => List.rev(toList(a))
                          (* Update our sentence *)
                        | SOME(_) => kthg :: List.rev(toList(a)) 
                    val res = fromList(List.rev(attach))
                  in res 
                  end) (kstart) (emptyseq)
      val res = makeSent
    in
      res
    end 

  fun makeParagraph (data : Data.kgramdata) (numSentences : int)
        ((low, high) : (int * int)) (seed : Rand.rand) : sentence seq =
        tabulate (fn i =>  let
              val newseed = Rand.fromInt(i)
              val (ri, rs) = Rand.randomInt newseed (SOME(low,high))
              val res = makeSentence data ri rs 
              in res end) numSentences
                             


end
