#include "cilkStars.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits>
#include <algorithm>
#include <string>

ppair cilk_friendly_stars(point *inp, int N) {
  
  float min_dist = std::numeric_limits<float>::infinity();
  ppair closest_pair;
  float track[N]; 
  ppair pairs[N]; // What will be initialized?
  float min_so_far = min_dist;

  cilk_for (int l = 0; l < N; l++) {
    track[l] = min_dist;
  } 
  
  cilk_for (int i=0; i<N; i++) {
    for (int j=i+1; j<N; j++) {
      point p = inp[i];
      point q = inp[j];
      float d = p.dist_squared(q);
      if (d < track[i]) {
        track[i] = d;
        pairs[i] = ppair(p,q);
      }
    }
  }

  for (int k = 0; k < N; k++) {
    if(track[k] < min_so_far) {
      min_so_far = track[k];
      closest_pair = pairs[k];
    }
  }

  return closest_pair;

}
