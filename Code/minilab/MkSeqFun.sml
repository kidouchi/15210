functor MkSeqFun (structure Seq : PARTIALSEQUENCE) : SEQFUN =
struct
  open Seq

  (* Remove this line when you're done *)
  exception NotYetImplemented
  exception BadHarmonics

  (* Task 5.3 *)
  fun allHarmonics (n : int) : real seq =
      if(n <= 0) then raise BadHarmonics
      else 
         let
            fun harnum (x : real, a : int) : real =
                  (1.0 / Real.fromInt(a)) + x
            val numseq = tabulate (fn i => i+2) n
            val (realseq, num) = iterh harnum 1.0 numseq
         in
            realseq
         end

  (* Task 5.4 *)
  fun groupedHarmonics (n : int) (k : int) : (int * real seq) seq =
      if((n < 0) orelse (k <= 0)) then raise BadHarmonics
      else 
         let
            val harms = allHarmonics(n)
            val hlen = length harms
            val splitnum = if (hlen mod k = 0) 
                           then 
                              hlen div k
                           else 
                              (hlen div k) + 1
            val splitseq = tabulate 
                           (fn i => if(i = splitnum-1) 
                                    then
                                       subseq harms (k*i, 
                                                    (length harms-k*i))
                                    else
                                       subseq harms (k*i, k)
                                 )  (splitnum)
         in
            enum(splitseq)
         end

  (* Task 5.6 *)
  (* Helper function fro printGroups *)
  (* printGroupsHelp : (int * real seq) seq -> unit *)
  fun printGroupsHelp (G : (int * real seq) seq) : unit =
      case showl(G) of
         NIL => print (">\n")
      |  CONS(n, G') =>
            let
               val (i, rs) = n
               fun printHelp (s : real seq) : unit =
                  case showl(s) of
                     NIL => print (">")
                  |  CONS(x, s') => (print (Real.toString(x) ^ ",");
                                     printHelp(s'))
            in                      
               (print ("(" ^ Int.toString(i) ^ ",<"); 
                        printHelp(rs); print ("), "); 
                        printGroupsHelp(G'))
            end

  fun printGroups (G : (int * real seq) seq) : unit =
      (print ("<"); printGroupsHelp(G))

end
