functor MkPartialArraySequence (structure BareSeq : BARESEQUENCE)
                                  : PARTIALSEQUENCE =
struct
  open BareSeq

  (* Remove this line when you're done *)
  (*exception NotYetImplemented *)

  (* Task 5.1 *)

  fun rev s =
      let val n = length s
      in tabulate (fn i => nth s (n-1-i)) n
      end

  fun map (f : 'a -> 'b)  (s : 'a seq) : 'b seq =
      tabulate (fn i => f(nth s i)) (length s)

  fun enum (s : 'a seq) : (int * 'a) seq =
      tabulate (fn i => (i, nth s i)) (length s)
      
  fun mapIdx (f : ((int * 'a) -> 'b)) (s : 'a seq) : 'b seq =
      tabulate (fn i => f(i, nth s i)) (length s) 

  fun append (s : 'a seq, t : 'a seq) : 'a seq =
      tabulate (fn i => if(i < length s) 
                        then nth s i
                        else nth t (i - length s)) (length s + length t)

  fun iter (f : 'b * 'a -> 'b) (b : 'b) (s : 'a seq) : 'b =
      if(length s = 0) then b
      else f(iter f b (take(s, length s - 1)), (nth s (length s - 1)))

  fun iterHelp (f :'b*'a->'b) (b:'b) (s:'a seq) : ('b list*'b) =
      case showl(s) of
         NIL => ([b], b)
      |  _ =>
            let
               val (blist, newb) = iterHelp f b (take(s, length s-1))
               val fb = f(newb, nth s (length s-1))
            in
               (fb :: blist, fb)
            end

   (*TODO : FINISH THIS *)
  fun iterh (f : 'b * 'a -> 'b) (b : 'b) (s : 'a seq) : ('b seq * 'b) =
      let
         val (blist, bval) = iterHelp f b s
         val newseq = drop(fromList(blist), 1)
      in
         (rev(newseq), bval)
      end 

  (* toListHelp : ('a seq * int) -> 'a list *)
  (* Helper function for toList *)
  fun toListHelp(s : 'a seq, i : int) : 'a list =
      if (i = length s) then []
      else (nth s i) :: toListHelp(s, i+1)

  fun toList (s : 'a seq) : 'a list =
      toListHelp(s, 0)
  
  (* Task 5.5 *)
  fun toString (f : 'a -> string) (s : 'a seq) : string =
      let
         val slist = toList(tabulate (fn i => f(nth s i)) (length s))
      in
         "<" ^ (String.concatWith (",") slist) ^ ">"
      end

end 
