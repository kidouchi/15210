4.1
	wordSplit : char seq -> int -> (char seq * char seq)
	Splits the word at i and returns the two halves

	fun SmartSpacer (S : char seq) (i : int) =
		let
			val (first, second) = wordSplit S i
			val addSpace = SmartSpacer first (i-1) 
			val noSpace = SmartSpacer S (i-1)
		in
			if(i = 0) then isWord(S)
			else 
				if(isWord(second)) then
					(addSpace orelse noSpace)	
				else
					noSpace
		end
			
4.2
	This is a function that finds Longest Increasing Subsequence
	LIS : int seq -> int seq 

 	fun frogDP (V1 : int seq, V2 : int seq) =
		let
			val tupleList = zip (V1) (V2)
			val Sorted = sort (fn ((a1,_),(a2,_) => Int.compare(a1,a2))) 
							tupleList
			val NewS = map (fn (_, x) => x) Sorted
		in
			LIS(NewS)
		end	

4.3
	Apply_Rule : char -> (char * char) seq
	Returns all possible rules applied to character 
 
 	allPrefixSuffix : string -> (string * string) seq
 	Returns all possible prefixes and suffixes for a given string

	fun HelpJimmy(start : char, target : string) : int =
		let
			fun HelpJimmy' (s, t) : int seq =
				let
					val pairs = Apply_Rule(s)
					val substrs = allPrefixSuffix(t)
				in
					if(start = target) then 0 
					for each (A, B) in pairs
						for each (P, S) in substrs
							add to Sequence ->
							(Int.Max(HelpJimmy(A, P), HelpJimmy(B, S)) + 1)
				end
		in
			Reduce Int.min (HelpJimmy' (start, target))
		end


