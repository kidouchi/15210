% You should title the file with a .tex extension (hw1.tex, for example)
\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{qtree}
\usepackage{amssymb}
\usepackage{fancyhdr}

\oddsidemargin0cm
\topmargin-2cm     %I recommend adding these three lines to increase the 
\textwidth16.5cm   %amount of usable space on the page (and save trees)
\textheight23.5cm  

\newcommand{\question}[2] {\vspace{.25in} \hrule\vspace{0.5em}
\noindent{\bf #1: #2} \vspace{0.5em}
\hrule \vspace{.10in}}
\renewcommand{\part}[1] {\vspace{.10in} {\bf (#1)}}

\newcommand{\myname}{Kelly Idouchi}
\newcommand{\myandrew}{kidouchi@andrew.cmu.edu}
\newcommand{\myhwnum}{3}  %HW #

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
 
\pagestyle{fancyplain}
\lhead{\fancyplain{}{\textbf{HW\myhwnum}}}      
\rhead{\fancyplain{}{\myname\\ \myandrew}}
\chead{\fancyplain{}{15-210}}

\begin{document}

\medskip                       
\thispagestyle{plain}
\begin{center}                  
{\Large 15-210 BigNumLab \myhwnum} \\ %TITLE
\myname \\
\myandrew \\
Section D\\
\today \\
\end{center}


\question{Task 5.1}

\part{1}

\begin{flushleft} \vspace{-0.3in}
	\item $T(n) = T(n-1) + \Theta(log(n))$
	\item Using the tree method such that $k_1$ is some constant, we get the following : 
		\begin{align*}
			&\sum_{i=1}^{n} k_{1} \cdot log(i)\\
			&= k_{1} \cdot (log(1) + log(2) + log(3) + . \ . \ . + log(n)) \\
			&= k_{1} \cdot log(1 \cdot 2 \cdot 3 . \ . \ . n) \\
			&= k_{1} \cdot log(n!) 
		\end{align*}		
	\item The tree method provides an exact summation so therefore the recurrence is $\in \Theta(log(n!))$

\end{flushleft}

\part{2}

\begin{flushleft} \vspace{-0.3in}
	\item Using the brick method, we get the following :
	\item The tree is balanced because on each level we are doing $n$ work.
	\item Determining how many times we can square root $n$ until we reach 2, which is the base case or leaf, can help us find the depth of the tree.
	\item If we were to solve $n^{1/2^i} = 2$ then we get that there are $log(log(n))$ levels.
		\begin{align*}
			n^{1/2^i} &= 2 \\
			1/2^i \cdot log(n) &= log(2) \\
			log(1/2^i) + log(log(n)) &= log(log(2)) \\
			i \cdot log(1/2) + log(log(n)) &= log(log(2)) \\
			-i + log(log(n)) &= 0 \\
			i &= log(log(n))
		\end{align*}
	\item So we have the root node which will have work of $n$ given by the $O(n)$ in the recurrence. 
	\item The root node will have $\sqrt{n}$ children and each children will have $\sqrt{n}$ work. So this level will have work $\sqrt{n} \cdot \sqrt{n} = n$. Each $\sqrt{n}$ children (They'll now be the current parent so we'll call them that from now on) will have $\sqrt{\sqrt{n}}$ children and each of them will be doing $\sqrt{\sqrt{n}}$ work.  So we have that each of the $\sqrt{n}$ parents will have its children doing a total work of $\sqrt{\sqrt{n}} \cdot \sqrt{\sqrt{n}} = \sqrt{n}$. So now the total work for that level is $\sqrt{n} \cdot \sqrt{n} = n$. This continues and so each level will have a work of $n$.
		
\end{flushleft} 

\part{3}

\begin{flushleft} \vspace{-0.3in}
	\item We'll be trying to prove that $T(n) \in O(n)$
	\item The guess was made by the following :
	\item By the tree method we get the following sum such that constants $k_{1}, k_{2}$ :
		\begin{align*} 
			&\sum_{i=0}^{log_{4}(n)} 4^i (k_{1} \cdot \sqrt{n/4^i}) + k_{2} \\
			&=\sum_{i=0}^{log_{4}(n)} 4^i(k_{1} \cdot \sqrt{n/4^i}) + \sum_{i=0}^{log_{4}(n)} k_2 \\
			&=k_{1} \cdot \sum_{i=0}^{log_{4}(n)} (4^i \cdot \sqrt{n/4^i}) + log_{4}(n) \cdot k_{2} \\
			&=k_{1} \cdot \sum_{i=0}^{log_{4}(n)} (\sqrt{4^i \cdot n}) + log_{4}(n) \cdot k_{2} \\
			&=k_{1} \cdot \sqrt{n} \cdot \sum_{i=0}^{log_{4}(n)} (\sqrt{4^i}) + log_{4}(n) \cdot k_{2} \\
			&=k_{1} \cdot \sqrt{n} (2 \cdot \sqrt{n} - 1) + log_{4}(n) \cdot k_{2} \\
			&= 2 k_{1} n - k_{1} \sqrt{n} + k_{2} log_{4}(n)
		\end{align*}
	\item After doing some math, we'll see that the recurrence is in $O(n)$
	\item Now, by the substitution method, we'll be trying to prove this guess is right :
	\item $log_{4}(n)$ becomes so small eventually that it's trivial, so we'll just make it $k_{2}$
	\item We want to show that $T(n) \leq 2 k_{1} n - k_{1} \sqrt{n} + k_{2}$
	\item \underline{Base Case} : 
	\item $T(1) \leq 2k_{1}(1) - k_{1}(1) + k_{2} \leq 2k_{1} - k_{1} + k_{2} \leq k_{1} + k_{2}$
	\item $T(1) = 1$ because $\sqrt{1} = 1$ and we know that there must be some $k_{1} + k_{2}$ that will be $\geq 1$. So, it holds.
	\item \underline{IH} : $T(m) \leq 2 k_{1} m - k_{1} \sqrt{m} + k_{2}$ for all $m \leq n, n > 1$
	\item \underline{IS} :
	\item Let $k_{1} = 5c, k_{2} = c$
		\begin{align*}
			T(n)  &= 4T(n/4) + c \cdot \sqrt{n} \\
				&= 4(2k_{1}(n/4) - k_{1}\sqrt{n/4}+k_{2}) + c \cdot \sqrt{n} \ \ [By \ IH] \\ 
				&= 2k_{1}(n) - 2k_{1}\sqrt{n} + 4k_{2} + c \cdot \sqrt{n} \\
				&= 2k_{1}(n) - 10c\sqrt{n} +  4c + c \cdot \sqrt{n} \\
				&= 2k_{1}(n) - 9c\sqrt{n} + 4c \\
				&= 2k_{1}(n) - 3c\sqrt{n} -6c\sqrt{n} + 4c \\
				&= 2k_{1}(n) - k_{1}\sqrt{n} -6k_{2}\sqrt{n} + 4k_{2} \\
				&\leq 2 k_{1} n - k_{1} \sqrt{n} + k_{2}
		\end{align*}
	\item Now we'll try to prove $\Omega(n)$ using the substitution method :
	\item We want to show that $T(n) \geq 2 k_{1} n - k_{1} \sqrt{n} + k_{2}$
	\item \underline{Base Case}:
	\item $T(1) \geq 2k_{1}(1) - k_{1}(1) + k_{2} \geq 2k_{1} - k_{1} + k_{2} \geq k_{1} + k_{2}$
	\item $T(1) = 1$ and there is some $k_{1}, k_{2}$ such that $k_{1}+k_{2}$ is $\leq 1$
	\item \underline{IH}: $T(m) \geq 2 k_{1} m - k_{1} \sqrt{m} + k_{2}$ for all $m \leq n, n > 1$
	\item \underline{IS}:
	\item let $k_{1} = c, k_{2} = c$
		\begin{align*}
			T(n)  &= 4T(n/4) + c \cdot \sqrt{n} \\
				&= 4(2k_{1}(n/4) - k_{1}\sqrt{n/4}+k_{2}) + c \cdot \sqrt{n} \ \ [By \ IH] \\ 
				&= 2k_{1}n - 2k_{1}\sqrt{n} + 4k_{2} + c \cdot \sqrt{n} \\
				&= 2k_{1}n - 2c\sqrt{n} + 4c + c \sqrt{n} \\
				&= 2k_{1}n - k_{1}\sqrt{n} + 4k_{2} \\
				&\geq 2 k_{1} n - k_{1} \sqrt{n} + k_{2} \\
		\end{align*}
	\item We have shown that it's in $O(n)$ and $\Omega(n)$

\end{flushleft}

		
\end{document}