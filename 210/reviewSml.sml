(* someFunc : int -> bar -> string *)

(* someOtherFunc : int * bar -> unit *)
fun someOtherFunc (foo : int, bar : string) : unit = ()


fun someFunc (foo : int) (bar : string) : string =
   let
      val foobar = (Int.toString foo) ^ bar
      val _ = print foobar
   in foobar
   end


val _ = someOtherFunc (1, "bar")
val "1bar" = someFunc 1 "bar"

datatype 'a tree = Empty | Node of 'a tree * 'a * 'a tree

val T : int tree = Node(Node(Empty, 2, Empty), 1, Empty)

(* Semicolon in SML *)
fun inorderPrint (T : 'a tree) (toString : 'a -> string) = 
   case T of
      Empty => ()
   |  Node(L, x, R) => (inorderPrint L toString;
                        print (toString x);
                        inorderPrint R toString)

val n = ~3 (* -3 *)

(* OP *)
val L = [1, 2, 3, 4];
val sum = List.foldl op+ 0 L;

(* Exception and Handling Exception *)
exception MyException
let val _ = (raise MyException) handle MyException => "hello"
                                    | AnotherException => 
                                    | _ =>

(* Printing *)
fun printTuple (a, b) = 
      print ("(" ^ Int.toString a ^ ", " ^ Int.toString b ^ ")\n")
      





