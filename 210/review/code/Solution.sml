structure Solution =
struct
  structure BareSeq = BareArraySequence
  structure RefPartSeq = RefMkPartialArraySequence (structure BareSeq = BareSeq)
  open RefPartSeq
  
  (* Problem 1 -- don't forget it's O(n)! *)
  fun fib (n : int) : int =
      let fun fib_tail (k, a, b) =
              case k of 
                0 => a
              | 1 => b
              | _ => fib_tail (k - 1, b, a + b)
      in fib_tail (n, 0, 1)
      end

  (* Problem 2 -- exceptional binary search *)  
  exception NotFound
  fun binsearch (S : int seq) (n : int) : int =
      case length S of 
        0 => raise NotFound
      | l => let val mid = nth S (l div 2)
                 val _ = print (toString Int.toString S ^ "\n")
             in case Int.compare(n, mid) of
                  EQUAL => l div 2
                | GREATER => (l + 1) div 2 + binsearch (drop (S, l - l div 2)) n
                | LESS => binsearch (take (S, l div 2)) n
             end

  (* Problem 3 -- 99 bottles of beer on the wall... *)
  fun bottlesOfBeer (n : int) : unit =
      if n > 0 then 
        let val cur = Int.toString n
            val next = Int.toString (n - 1)
        in
          (print (cur ^ " bottles of beer on the wall\n");
           print (cur ^ " bottles of beer\n");
           print "Take one down, pass it around\n";
           print (next ^ " bottles of beer on the wall\n\n");
           bottlesOfBeer(n - 1))
        end
      else ()

  (* Problem 4 -- I extended the PartialArraySequence sig to
   * include the zip and flatten functions which you may find useful.
   *)
  fun rangeExpand (S : int seq) : int seq = 
      let val pairs = zip S (drop (S, 1))
          val ranges = map (fn (a, b) => tabulate
                                             (fn i => if a < b then i + a
                                                      else a - i)
                                             (Int.abs(a - b))
                           ) pairs
      in append(flatten ranges, singleton (nth S (length S - 1)))
      end
end
