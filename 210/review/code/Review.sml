structure Review = 
struct
  structure BareSeq = BareArraySequence
  structure RefPartSeq = RefMkPartialArraySequence (structure BareSeq = BareSeq)
  open RefPartSeq

  exception Unimplemented
  exception NotFound

  (* Problem 1 -- don't forget it's O(n)! *)
  fun fib (n : int) : int =
      let fun fibHelper (n, a, b) =
         if n = 0 then a
         else fibHelper(n-1, b, a + b)
      in
         fibHelper(n, 0, 1)
      end

  (* Problem 2 -- exceptional binary search *)  
  fun binsearch (S : int seq) (n : int) : int =
      let
         val cmp = nth S (length S div 2)
         val index = length S div 2
      in
         if(length S = 0) then raise NotFound 
         else if(cmp > n) then
            binsearch (subseq S (0, index)) n
         else if (cmp < n) then
            binsearch (subseq S (index, length S)) n
         else n
      end

  (* Problem 3 -- 99 bottles of beer on the wall... *)
  fun bottlesOfBeer (n : int) : unit =
      case n of 
         0 => print ("0 bottles of beer on the wall")
      |  n => (print (Int.toString n ^ " bottles of beer on the wall\n" ^ 
                     Int.toString n ^ " bottles of beer\n" ^ 
                     "Take one down, pass it around\n");  
                     bottlesOfBeer(n-1))

  (* Problem 4 -- I extended the PartialArraySequence sig to
   * include the zip and flatten functions which you may find useful.
   *)
  fun rangeExpand (S : int seq) : int seq =
      raise NotFound

end


structure Tester =
struct
  open Review
  val % = fromList

  fun testFib () =
      let 
        val 0 = fib 0
        val 2 = fib 3
        val 5 = fib 5
      in () end
                
  fun testBinSearch () =
      let
        val S = %[1, 2, 4, 5, 6]
        val 0 = binsearch S 1
        val 3 = binsearch S 5
        val 4 = binsearch S 6
        val NONE = SOME(binsearch S 3) handle _ => NONE
      in () end
   
   fun testRangeExpand () =
       let val S = %[~3, 1, ~1]
           fun eq (S, T) = iter (fn (a, b) => a andalso b)
                               true 
                               (map (fn (a, b) => a = b) (zip S T))
           val true = eq (rangeExpand S, %[~3, ~2, ~1, 0, 1, 0, ~1])
       in () end
end
