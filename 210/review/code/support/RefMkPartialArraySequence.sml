functor RefMkPartialArraySequence (structure BareSeq : BARESEQUENCE)
                                     : PARTIALSEQUENCE =
struct
  open BareSeq

  fun rev s =
      let val n = length s
      in tabulate (fn i => nth s (n-1-i)) n
      end

  (* Brought up here because functions below need it *)
  fun toList s = List.tabulate (length s, (fn i => nth s i))

  fun map f s = fromList (List.map f (toList s))

  fun enum s =
      let
        fun enum' i nil = nil
          | enum' i (x::xs) = List.@ ([(i,x)], (enum' (i+1) xs))
      in fromList (enum' 0 (toList s))
      end

  fun mapIdx f s =
      let
        fun mapIdx' i nil = nil
          | mapIdx' i (x::xs) = List.@ ([f (i,x)], (mapIdx' (i+1) xs))
      in fromList (mapIdx' 0 (toList s))
      end

  fun append (s, t) = fromList (List.@ (toList s, toList t))

  fun iter f b s =
      let
        val n = length s
        val ss = tabulate (fn i => take (s, i)) (n+1)
        fun each t =
            List.foldl (fn (a,b) => f (b,a)) b (toList t)
        val all = List.map each (toList ss)
      in List.nth (all, n)
      end

  fun iterh f b s =
      let
        val n = length s
        val ss = tabulate (fn i => subseq s (0, i)) (n+1)
        fun each t =
            List.foldl (fn (a,b) => f (b,a)) b (toList t)
        val all = map each ss
      in (take (all, n), nth all n)
      end

  fun toString f s =
      let
        fun interComma (str, e) = str ^ (f e) ^ ","
        val res = iter interComma "" s
        val right = Int.max(0, String.size res - 1)
      in
        "<" ^ String.substring (res, 0, right) ^ ">"
      end

  local
    fun scan' f S =
        if length S = 1 then (empty (), nth S 0)
        else let
          val n = length S
          fun contract i =
              if i = n div 2 then nth S (2*i)
              else f (nth S (2*i), nth S (2*i + 1))
          val S' = tabulate contract ((n+1) div 2)
          val (R, res) = scan' f S'
          fun expand 0 = nth S 0
            | expand i =
              if i mod 2 = 1 then nth R (i div 2)
              else f (nth R ((i-1) div 2), nth S i)
        in (tabulate expand (n-1), res)
        end
  in
    (* scan (2) combines base case after recursion *)
    fun scan f b S =
        if length S = 0 then (empty (), b)
        else let
          val (R, res) = scan' f S
          val R' = map (fn x => f (b, x)) R
        in (append (singleton b, R'), f (b, res))
        end
  end

  fun zip s t =
      tabulate (fn i => (nth s i, nth t i)) (Int.min (length s, length t))

  fun flatten ss =
      if length ss = 0 then empty()
      else let
          val (starts, n) = scan op+ 0 (map length ss)
          val arr = Array.array (n, NONE)
          fun write i (j,x) = Array.update (arr, i+j, SOME x)
          val _ = map (fn (i,s) => mapIdx (write i) s) (zip starts ss)
        in tabulate (fn i => valOf (Array.sub(arr, i))) n
        end
end 
